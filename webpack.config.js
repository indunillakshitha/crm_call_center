const path = require('path');

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
            MIX_IMAGE_LOCATION : '/images/',
        },
    },
};
