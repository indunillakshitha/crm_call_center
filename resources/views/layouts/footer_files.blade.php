<script src="{{asset('assets/node_modules/jquery/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="{{asset('assets/node_modules/popper/popper.min.js')}}"></script>
<script src="{{asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('assets/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('assets/dist/js/waves.js')}}"></script>
<!--Menu sidebar -->
{{-- <script src="{{asset('assets/dist/js/sidebarmenu.js')}}"></script> --}}
<!--Custom JavaScript -->
<script src="{{asset('assets/dist/js/custom.min.js')}}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--Sky Icons JavaScript -->
<script src="{{asset('assets/node_modules/skycons/skycons.js')}}"></script>
<!--morris JavaScript -->
{{-- <script src="{{asset('assets/node_modules/raphael/raphael-min.js')}}"></script> --}}
{{-- <script src="{{asset('assets/node_modules/morrisjs/morris.min.js')}}"></script> --}}
<script src="{{asset('assets/node_modules/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<!-- Chart JS -->
{{-- <script src="{{asset('assets/dist/js/dashboard4.js')}}"></script> --}}

<script src="{{asset('assets/node_modules/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>

<script src="{{asset('assets/dist/js/datatable/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/dist/js/datatable/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/dist/js/datatable/jszip.min.js')}}"></script>
<script src="{{asset('assets/dist/js/datatable/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/dist/js/datatable/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/dist/js/datatable/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/dist/js/datatable/buttons.print.min.js')}}"></script>

{{-- <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script> --}}

<script src={{asset("/assets/node_modules/moment/moment.js")}}></script>
<script src={{asset("/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js")}}></script>
