export const MyFunctions = {

    show_and_remove_img_input : function(event){

        const files = event.target.files
        const fileReader = new FileReader()

        fileReader.addEventListener('load', ()=>{
            selected_image.src = fileReader.result
        })
        fileReader.readAsDataURL(files[0])
        remove_selected_img_btn.classList.remove('d-none')
    },

    remove_selected_image : function() {
        selected_image.src = null
        img_input.value = null
        remove_selected_img_btn.classList.add('d-none')
    },


}
