import firebase from 'firebase';
import firestore from 'firebase/firestore'
 
const firebaseConfig = {
    apiKey: "AIzaSyDq2uHq9dxh8C4Kr4BUZaYS0dh33uDT_0g",
    authDomain: "duthaya-lk.firebaseapp.com",
    databaseURL: 'https://duthaya-lk-default-rtdb.firebaseio.com/',
    projectId: "duthaya-lk",
    storageBucket: "duthaya-lk.appspot.com",
    messagingSenderId: "449780078290",
    appId: "1:449780078290:web:8334dd0a6848a68fc07f0b",
    measurementId: "G-VV6QGFVLJR"
  };

 const firebaseApp = firebase.initializeApp(firebaseConfig);

// export default firebaseApp;
export default firebaseApp.firestore();

// import firebase from 'firebase'
// import firestore from 'firebase/firestore'

// var firebaseConfig = {
//   apiKey: "AIzaSyD_6dl0wYOBwzxHIwYnZlsP0IwlBLyocJA",
//   authDomain: "vue-firebase-auth-geo-007.firebaseapp.com",
//   databaseURL: "https://vue-firebase-auth-geo-007.firebaseio.com",
//   projectId: "vue-firebase-auth-geo-007",
//   storageBucket: "vue-firebase-auth-geo-007.appspot.com",
//   messagingSenderId: "815664905503",
//   appId: "1:815664905503:web:bf9b0fd3a145158868842a"
// };
// // Initialize Firebase
// const firebaseApp= firebase.initializeApp(firebaseConfig);

// export default firebaseApp.firestore()