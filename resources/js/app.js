require('./bootstrap');


// Import modules...
import Vue from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue';
import PortalVue from 'portal-vue';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import VueMask from 'v-mask';

import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';


Vue.mixin({ methods: { route } });
Vue.use(InertiaPlugin);
Vue.use(PortalVue);
Vue.use(VueSweetalert2);
Vue.use(VueMask);
Vue.component('v-select', vSelect)
Vue.component('pagination', require('laravel-vue-pagination'));


const app = document.getElementById('app');

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
        mounted(){

        },
        methods:{
            hasPermission(str){
                if(this.$page.props.permission==undefined){
                    return false;
                }
                 if((this.$page.props.permission[str]) != undefined){
                     return this.$page.props.permission[str];
                 }else{
                     return false;
                 }
            },

            show_swal(text, icon){
                return this.$swal({
                    toast: true,
                    // title ,
                    text ,
                    icon ,
                    position: 'top-end',
                    timer: 3000,
                    timerProgressBar: true,
                });
            },

            delete_alert(url, form, id, dataTable){
                // console.log(id);
                // console.log(form);
                // dataTable.reloadDatatable();
                this.$swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                .then((result) => {
                    if (result.isConfirmed) {
                       form.id = id;
                       form.delete(route(url), {
                                preserveScroll: true,
                                onSuccess: () => {
                                    dataTable.reloadDatatable();
                                    this.show_swal('Record Deleted successfully!','success')
                                },
                                onError: () => {
                                    this.show_swal('Something went wrong!', 'error')
                                },
                                onFinish: () => form.reset(),
                            })
                    }
                })
            }

        }
}).$mount(app);
