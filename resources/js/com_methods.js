export default function(Vue) {
    Vue.auth = {
        //check Permissions
        hasPermission(str){
            if(this.props.permission[str]){
                return this.$page.props.permission[str];
            }else{
                return false;
            }
        },

    }

    Object.defineProperties(Vue.prototype, {
        $auth: {
            get: () => {
                return Vue.auth
            }
        }
    })
}