import * as functions from 'firebase-functions' 
import * as admin from  'firebase-admin';
admin.initializeApp();

const Firestore = admin.firestore;
const db = Firestore();

const axios = require("axios");
const cors = require("cors")({ origin: true});

const googleMapApiKey = "AIzaSyDq2uHq9dxh8C4Kr4BUZaYS0dh33uDT_0g";
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.searchPlaces = functions.https.onRequest( async (request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
    try{

        const place_string = request.body.place_string;

        const {data} = await axios.get(
            `https://maps.googleapis.com/maps/api/place/findplacefromtext/output?input=${place_string}&key=${googleMapApiKey}`
        
            );

        if(data.status!=="OK"){
            return cors(request, response, ()=> {
                response.status(200).send("No Results");
            })
        }

        const places = data.result[0];

        response.status(200).send(places);
        // const objPlaces = {
        //     address: places.formatted.address,
        //     geoPoin: new admin.firestore.GeoPoint(places.)
        // }

    }catch(error) {
        functions.logger.error(error.message);
        response.status(500).send();
    }
  //response.send(500);
});
