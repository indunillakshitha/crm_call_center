<?php

namespace App\Models;

use App\Models\User;
use App\Models\Channel;
use App\Models\OrderedItems;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;
    protected $fillable =[
        'order_number',
        'customer_id', 
        'order_date',
        'deliver_date',
        'source',
        'channel_id',
        'show_time_id',
        'status',
        'status_id',
        'executive_ramarks',
        'agent_id',
        'contact_1',
        'contact_2',
        'address',
        'area_id',
        'district_id',
        'remark',
        'is_hold',
    ];

    /**
     * Get all of the comments for the Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderedItems()
    {
        return $this->hasMany(OrderedItems::class);
    }

    /**
     * Get the user that owns the Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function callAgent()
    {
        return $this->belongsTo(User::class, 'agent_id');
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    
    public function orderItem()
    {
        return $this->hasMany(OrderedItems::class);
    }
    
}
