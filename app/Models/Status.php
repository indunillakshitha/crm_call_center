<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    public static function getStatusbadge($status)
    {   
        
        $html='';

        if($status=='INQUIRY'){
            $html ='<span class="badge badge-pill badge-cyan ml-auto">INQUIRY</span>';
        }
        if($status=='NEW'){
            $html ='<span class="badge badge-pill badge-info ml-auto">NEW</span>';
        }
        if($status=='PENDING'){
            $html ='<span class="badge badge-pill badge-warning ml-auto">PENDING</span>';
        }
        if($status=='IN TRANSIT'){
            $html ='<span class="badge badge-pill badge-dark ml-auto">IN TRANSIT</span>';
        }
        if($status=='DELIVERED'){
            $html ='<span class="badge badge-pill badge-success ml-auto">DELIVERED</span>';
        }
        if($status=='COMPLETED'){
            $html ='<span class="badge badge-pill badge-purple ml-auto">NEW ORDER</span>';
        }
        if($status=='CANCELLED'){
            $html ='<span class="badge badge-pill badge-danger ml-auto">CANCELLED</span>';
        }
        return $html;
    }
}
