<?php

namespace App\Models\Utility;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaveImgUtility extends Model
{
    use HasFactory;

    static function save_img($record, $img, $add_to_path){
        // $img = $request->file('img');
        $record->img = env('IMAGE_LOCATION').$add_to_path.'/'. $img->getClientOriginalName() . time() . '.' . $img->extension();

        $path = env('IMAGE_LOCATION').$add_to_path;
        $img->move(public_path($path), $record->img);

        return $record->save();
    }
}


