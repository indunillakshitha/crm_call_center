<?php

namespace App\Models\Utility;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionsUtility extends Model
{
    use HasFactory;

    static function checkPermission($permission){

        if(!auth()->user()->can($permission)){
            return abort(403, 'Unauthorized action.');
        }

    }
}
