<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShowTimes extends Model
{
    use HasFactory;

    protected $fillable = [
        'channel_id',
        'start_timestamp',
        'end_timestamp',
        'status',
    ];

    
}
