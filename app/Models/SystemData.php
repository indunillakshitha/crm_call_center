<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemData extends Model
{
    use HasFactory;

    protected $fillable =['key','value'];

    public static function getProperty($property)
    {
        $val = self::where('key',$property)->first();
        return isset($val) ? $val->value : null;
    }

    public static function addProperty($key,$val)
    {
        $val = self::create(['key'=>$key, 'value'=>$val]);
        return isset($val) ? $val->value : null;
    }

    public static function hasUpdate($property)
    {
        $db_val = self::getProperty($property);
        if((double)($db_val) > config('app.'.$property)){
            return true;
        }else{
            return false;
        }
    }
    
}
