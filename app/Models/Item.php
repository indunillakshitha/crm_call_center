<?php

namespace App\Models;

use App\Models\OrderedItems;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Item extends Model
{
    use HasFactory;
    protected $fillable = [
        'item_name', 'item_description', 'item_price'
    ];

    /**
     * Get all of the comments for the Item
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderedItems(): HasMany
    {
        return $this->hasMany(OrderedItems::class);
    }
}
