<?php

namespace App\Http\Controllers;

use App\Models\ShowTimes;
use Illuminate\Http\Request;

class ShowTimesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShowTimes  $showTimes
     * @return \Illuminate\Http\Response
     */
    public function show(ShowTimes $showTimes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShowTimes  $showTimes
     * @return \Illuminate\Http\Response
     */
    public function edit(ShowTimes $showTimes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShowTimes  $showTimes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShowTimes $showTimes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShowTimes  $showTimes
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShowTimes $showTimes)
    {
        //
    }
}
