<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Area;
use App\Models\Item;
use App\Models\User;
use Inertia\Inertia;
use App\Models\Order;
use App\Models\Status;
use App\Models\Channel;
use App\Models\Inquiry;
use App\Models\Customer;
use App\Models\District;
use App\Models\ShowTimes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class InquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function validateRequest(Request $request){

        $request->validate([
            'customer_id' => 'required',
            'customer_name' => 'required',
            'delivery_date' => 'required',
            'source' => 'required',
            'qt' => 'required',
            'channel' => 'required',
            'show_time' => 'required',
            'executive_remarks' => 'required',
            'contact_1' => 'required',
            'contact_2' => 'required',
            'address' => 'required',
            'area' => 'required',
            'district' => 'required',
            // 'remarks' => 'required',
        ]);
    }

    public function index()
    {
        // return Inertia::render('Inquiry/index');
        $new_orders =  Order::where('orders.status', 'INQUIRY')->get();
        $all_statuses = Status::all();
        $districts=District::all();
        $all_areas=Area::all();
        $all_agents = User::role(config('custom_config.default_delivery_agent_role'))->get();
        return Inertia::render('Order/ViewOrders', [
            'table_buttons' => ['csv', 'excel', 'pdf', 'print'],
            // 'data_table_route' => 'order.getdata',
            'status' => 'INQUIRY',
            'new_orders' => $new_orders,
            'all_statuses' => $all_statuses,
            'all_agents' => $all_agents,
            'districts' => $districts,
            'all_areas' => $all_areas,
            'status_id'=>1

        ]);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_products = Item::all();
        $all_channels = Channel::all();
        $all_showtimes = ShowTimes::where('status', 1)->get();
        $all_districts = District::all();
        $all_areas = Area::all();
        return Inertia::render('Order/CreateOrder', compact('all_products', 'all_channels', 'all_showtimes' , 'all_districts','all_areas'));
        // return Inertia::render('Inquiry/CreateUpdate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validateRequest($request);

        return 'validated';

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Inquiry $inquiry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function edit(Inquiry $inquiry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inquiry $inquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inquiry $inquiry)
    {
        //
    }

    public function checkPhoneNo(Request $request){


        $existing_customer = Customer::where('mobile', $request->phone_no)->first();


        if($existing_customer){
            $all_products = Item::all();
            return Inertia::render('Order/Create', compact('all_products', 'existing_customer'));

        } else {
            return redirect(route('customer.create'));
        }

    }
    public function getData()
    {

         $orders = Order::join('customers','customers.id','=','orders.customer_id')
         ->join('areas','areas.id','=','orders.area_id')
         ->select('orders.*',
            'customers.first_name','customers.last_name'
            ,'areas.area_name as area');

        // if($status != 'get_all_statuses'){
        //     $orders = $orders->where('status', $status);
        // }

        return DataTables::of($orders)
            ->addColumn('action', function ($row) {
                return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item text-danger action_view" data-order-id="' . $row->id . '" href="javascript:void(0)" ><i class="fas fa-eye mr-2" ></i> VIEW</a>
                                </div>
                            </div>';
            })->editColumn('customer_name', function ($row) {
                return $row->first_name.' '.$row->last_name;
            })->editColumn('status', function ($row) {
                return Status::getStatusbadge($row->status);
            })
            ->rawColumns(['action','customer_name','status'])
            ->make(true);
    }

    public function getCustomerOrderHistory($customer_id)
    {   
        //$customer_id = $request->customer_id;
         $orders = Order::with(['callAgent','orderItem.item'])->where('customer_id', $customer_id);

        // if($status != 'get_all_statuses'){
        //     $orders = $orders->where('status', $status);
        // }

       // \Log::emergency($orders);

        return DataTables::of($orders)
            ->addColumn('action', function ($row) {
                return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item text-danger action_view" data-order-id="' . $row->id . '" href="javascript:void(0)" ><i class="fas fa-eye mr-2" ></i> VIEW</a>
                                </div>
                            </div>';
            })->addColumn('details', function($row) {
                $html='<table class="custom_table" width="100%"><tboday>';
                foreach($row->orderItem as $item){
                    $html .= '<tr>
                            <td width="50%" >'.$item->item->item_name.'</td>
                            <td width="10%" class="text-center">'.$item->product_qty.'</td>
                            <td width="15%" class="text-right">'.number_format($item->product_each_price,2,'.','').'</td>
                            <td width="25%" class="text-right">'.number_format($item->product_each_price* $item->product_qty,2,'.',',').'</td>
                        </tr>';
                }
                $html.='</tboday></table>';
                return $html;            
            })
            ->editColumn('order_total', function($row) {
                return number_format($row->order_total,2,'.',',');           
            })
            ->editColumn('status', function($row) {                 
                return Status::getStatusbadge($row->status);           
            })
            
            ->editColumn('agent', function($row) {
               return $row->callAgent->name;
            })
            ->rawColumns(['details','action','status'])
            ->make(true);
    }
}
