<?php

namespace App\Http\Controllers;

use Exception;
use DataTables;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\Item;
use App\Models\User;
use Inertia\Inertia;
use App\Models\Order;
use App\Models\Status;
use App\Models\Channel;
use App\Models\Customer;
use App\Models\District;
use App\Models\ShowTimes;
use App\Models\OrderedItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function validateOrder(Request $request)
    {
        return $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            // 'delivery_date' => ['required'] ,
            'source' => ['required'],
            // 'qt' => ['required'] ,
            // 'channel' => ['required'] ,
            // 'show_time' => ['required'] ,
            // 'executive_remarks' => ['required'] ,
            'contact_1' => ['required'],
            // 'contact_2' => ['required'] ,
            'address' => ['required'],
            // 'nic' => ['required', 'unique:customers'] ,
            // 'area' => ['required'] ,
            // 'district' => ['required'] ,
            // 'remarks' => ['required'] ,
            // 'added_items' => ['required'] ,
        ]);
    }

    public function index()
    {
        $items = Item::where('agent_id', Auth::user()->id)->get();
        return Inertia::render('Order/Index', ['items' => $items]);
    }


    public function create()
    {
        $all_products = Item::all();
        $all_channels = Channel::all();
        $all_showtimes = ShowTimes::where('status', 1)->get();
        $all_districts = District::all();
        $all_areas = Area::all();
        return Inertia::render('Order/Create', compact('all_products', 'all_channels', 'all_showtimes', 'all_districts', 'all_areas'));
    }

    public function store(Request $request)
    {
        // return $request;
        $this->validateOrder($request);
        //return $request->all();

        try {
            DB::beginTransaction();
            $data = $request;
            if ($data['ordered'] == '0') {

                $data['status'] = 'INQUIRY';
                $data['status_id'] = 1;
            } else {

                $data['status'] = 'NEW';
                $data['status_id'] = 2;
            }

            $data['agent_id'] = Auth::user()->id;
            $data['agent_name'] = Auth::user()->name;

            if (!Customer::where('id', $request->customer_id)->first()) {

                $customer['first_name'] = $request->first_name;
                $customer['last_name'] = $request->last_name;
                $customer['nic'] = $request->nic;
                $customer['address'] = $request->address;
                $customer['mobile'] = $request->contact_1;
                $customer['is_active'] = 1;

                try {
                    $customer_id = Customer::create($customer)->id;
                } catch (Exception $e) {
                    return $e;
                }
                $data['customer_id'] = $customer_id;
            }
            // return 1;
            $row_data = [];
            $order_total = 0;

            $order = Order::create([
                'customer_id' => $data->customer_id,
                'order_date' => Carbon::now(),
                'deliver_date' => $data->deliver_date,
                'source' => $data->source,
                'channel_id' => $data->channel,
                'show_time_id' => $data->show_time,
                'status' => $data['status'],
                'status_id' => $data['status_id'],
                'executive_ramarks' => $data->executive_remarks,
                'agent_id' => Auth::user()->id,
                'contact_1' => $data->contact_1,
                'contact_2' => $data->contact_2,
                'address' => $data->address,
                'area_id' => $data->area,
                'district_id' => $data->district,
                'remark' => $data->remarks,
                'is_hold' => null,
            ]);


            foreach ($request->added_items as $row) {
                $row_data[] = [
                    // 'order_id' => $order->id,
                    'item_id' => $row['id'],
                    'product_qty' => $row['item_qty'],
                    'unit_price' => $row['item_price'],
                ];
                $order_total += $row['item_price'];
            }
            $order->orderedItems()->createMany($row_data);
             $order->order_total = $order_total;
             $order->save();
            DB::commit();
            // return 2;

            return redirect()->route('inquiry.create');
        } catch (Exception $e) {
            \Log::error($e);
            DB::rollBack();
            abort(500);
        }
    }


    public function show(Order $order)
    {
        //
    }

    public function edit(Order $order)
    {
        //
    }


    public function update(Request $request, Order $order)
    {
        //
    }


    public function destroy(Order $order)
    {
        //
    }


    public function newOrders()
    {
        $districts = District::all();
        $all_areas = Area::all();

        $new_orders =  Order::where('orders.status', 'NEW')->get();
        $all_statuses = Status::all();
        $all_agents = User::role(config('custom_config.default_delivery_agent_role'))->get();
        return Inertia::render('Order/ViewOrders', [
            'table_buttons' => ['csv', 'excel', 'pdf', 'print'],
            // 'data_table_route' => 'order.getdata',
            'status' => 'NEW',
            'status_id' => 2,
            'new_orders' => $new_orders,
            'all_statuses' => $all_statuses,
            'all_agents' => $all_agents,
            'districts' => $districts,
            'all_areas' => $all_areas,

        ]);
    }
    public function pendingOrders()
    {
        $districts = District::all();
        $all_areas = Area::all();


        $new_orders =  Order::where('orders.status', 'PENDING')->get();
        $all_statuses = Status::all();
        $all_agents = User::role(config('custom_config.default_delivery_agent_role'))->get();
        return Inertia::render('Order/ViewOrders', [
            'table_buttons' => ['csv', 'excel', 'pdf', 'print'],
            'status' => 'PENDING',
            'status_id' => 3,
            'new_orders' => $new_orders,
            'all_statuses' => $all_statuses,
            'all_agents' => $all_agents,
            'districts' => $districts,
            'all_areas' => $all_areas,
        ]);
    }
    public function dispatchedOrders()
    {
        $districts = District::all();
        $all_areas = Area::all();

        $new_orders =  Order::where('orders.status', 'DISPATCHED')->get();
        $all_statuses = Status::all();
        $all_agents = User::role(config('custom_config.default_delivery_agent_role'))->get();
        return Inertia::render('Order/ViewOrders', [
            'table_buttons' => ['csv', 'excel', 'pdf', 'print'],
            'status' => 'DISPATCHED',
            'status_id' => 4,
            'new_orders' => $new_orders,
            'all_statuses' => $all_statuses,
            'all_agents' => $all_agents,
            'districts' => $districts,
            'all_areas' => $all_areas,
        ]);
    }
    public function deliveredOrders()
    {
        $districts = District::all();
        $all_areas = Area::all();

        $new_orders =  Order::where('orders.status', 'DELIVERED')->get();
        $all_statuses = Status::all();
        $all_agents = User::role(config('custom_config.default_delivery_agent_role'))->get();
        return Inertia::render('Order/ViewOrders', [
            'table_buttons' => ['csv', 'excel', 'pdf', 'print'],
            'status' => 'DELIVERED',
            'new_orders' => $new_orders,
            'status_id' => 5,
            'all_statuses' => $all_statuses,
            'all_agents' => $all_agents,
            'districts' => $districts,
            'all_areas' => $all_areas,
        ]);
    }
    public function completededOrders()
    {
        $districts = District::all();
        $all_areas = Area::all();

        $new_orders =  Order::where('orders.status', 'COMPLETED')->get();
        $all_statuses = Status::all();
        $all_agents = User::role(config('custom_config.default_delivery_agent_role'))->get();
        return Inertia::render('Order/ViewOrders', [
            'table_buttons' => ['csv', 'excel', 'pdf', 'print'],
            'status' => 'COMPLETED',
            'status_id' => 6,
            'new_orders' => $new_orders,
            'all_statuses' => $all_statuses,
            'all_agents' => $all_agents,
            'districts' => $districts,
            'all_areas' => $all_areas,
        ]);
    }
    public function getData($status)
    {

        if ($status == 'get_all_statuses') {
            $orders = Order::all();
        } else {
            $orders = Order::where('orders.status', $status)
                ->get();
        }

        return DataTables::of($orders)
            ->addColumn('action', function ($row) {
                return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item text-danger action_view" data-order-id="' . $row->id . '" href="javascript:void(0)" ><i class="fas fa-eye mr-2" ></i> VIEW</a>
                                </div>
                            </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function getData2(Request $request)
    {



        // echo $request->status;
        //return $request->all();

        ### WORKS LIKE A CHARM
        // $orders = Order::with('orderedItems.item','customer')
        // ->whereHas('customer', function($q)  {
        //     $q->where('first_name', 'user');
        //     // $q->where('last_name', '1');
        // })
        // // ->whereHas('orderedItems.item', function($q) {  //check hasManyThrough -> Laravel Daily YouTube Channel
        // //     $q->where('item.id', 8);
        // // })
        // ->where('orders.district', 1)
        // ->get();
        // return $orders;



        ## NOW USING LEFTJOINS
        $orders = Order::with('orderedItems.item')
            ->join('customers', 'customers.id', 'orders.customer_id')
            ->join('areas', 'areas.id', 'orders.area_id')
            ->where('orders.status_id', $request->status);
            // ->where('orders.status', 'NEW');


        if (isset($request->customer_id)) {
            $orders = $orders->where('orders.customer_id', $request->customer_id);
        }
        if ($request->district != '') {
            $orders = $orders->where('orders.district_id', $request->district);
        }
        if ($request->area != '') {
            $orders = $orders->where('area_id', $request->area);
        }

        if($request->start_date != ''  && $request->end_date != '' ){
            $orders= $orders->whereBetween('order_date', [$request->start_date ,$request->end_date]);
        }

        $paginate_by = $request->paginate_by;
        if ($paginate_by == 'ALL') {
            $paginate_by = count($orders->get());
        }
        $orders->select('orders.*', DB::raw('CONCAT(customers.first_name," ", customers.last_name) as customer_name'), 'customers.address', 'customers.nic', 'customers.mobile','areas.area_name');

        $orders =  $orders->paginate($paginate_by);
        // $orders =  $orders->paginate(5);
        return $orders;
    }


    public function addItems(Request $request)
    {

        try {
            DB::beginTransaction();
            OrderedItems::create($request->all());
            $order = Order::where('id', $request->order_id)->first();
            $order->order_total += $request->product_total;
            $order->save();
            DB::commit();
            return response()->json('Success');
        } catch (Exception $e) {
            DB::rollBack();
        }
    }
    public function removeItems(Request $request)
    {
        // return response()->json($request->data);
        try {
            DB::beginTransaction();
            OrderedItems::where('order_id', $request->order_id)
                ->where('product_id', $request->product_id)->delete();
            $order = Order::where('id', $request->order_id)->first();
            $order->order_total -= $request->product_total;
            $order->save();
            DB::commit();

            return response()->json('Success');
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    // public function getOrderDetails(Request $request)
    public function getOrderDetails(Request $request)
    {



        $orders = Order::leftjoin('ordered_items', 'ordered_items.order_id', 'orders.id')
            ->leftjoin('items', 'items.id', 'ordered_items.product_id')
            ->where('orders.id', $request->id)
            // ->where('orders.status',$status)
            ->select(
                'orders.*',
                'ordered_items.product_id',
                'ordered_items.product_qty',
                'items.item_name',
                'items.item_price'
            )
            ->get();

        return $orders;
    }
    public function forwardOrder(Request $request)
    {

        Order::where('id', $request->id)->update(['status' => $request->status]);
    }
    public function inquiryToOrder(Request $request)
    {
        // return $request;

        Order::where('id', $request->order_id)->update(['status' => $request->status]);
        return response()->json(['success' => 'Updated Success']);
    }

    public function changeStatus(Request $request)
    {
        // return $request->selected_row['id'];
        $order = Order::where('id', $request->selected_row['id'])->first();
        $order->update([
            'status_id' => $request->selected_option,
        ]);
        if ($request->selected_agent != '') {
            $order->update([
                'agent_id' => $request->selected_agent,
            ]);
        }


        return response()->json('success');
    }

    public function changeStatusBulk(Request $request)
    {


        // return $request->selected_agent_bulk;
        foreach ($request->selected_rows as $row) {
            $order = Order::where('id', $row['value'])->first();
            $order->update([
                'status' => $request->bulk_change_selected_status,
            ]);
            if ($request->selected_agent_bulk != '') {
                // return 123;
                $order->update([
                    'agent_id' => $request->selected_agent_bulk,
                ]);
            }
        }

        return Order::where('orders.status', $request->current_default_status)->paginate($request->paginate_by);
        // return response()->json(Order::where('orders.status', $request->current_default_status)->get());

    }

    public function ordersSearchField(Request $request)
    {
        // return Order::select($request->label)->where($request->label, 'like' , '%'.$request->search.'%')->get();
        if ($request->label == 'first_name') {
           return Customer::select('id','first_name','last_name','mobile','land_phone'
            //DB::raw('CONCAT(first_name," ",last_name," -( ", mobile," /"," ",land_phone," )") AS first_name')
           )
           ->where('mobile', 'like', '%' . $request->search . '%')
           ->orWhere('land_phone', 'like', '%' . $request->search . '%')
            ->orWhere($request->label, 'like', '%' . $request->search . '%')->get();
        }
        if($request->label == 'district'){
            return Order::select($request->label)->distinct()->where($request->label, 'like', '%' . $request->search . '%')->get();
        }

    }
    public function reltest()
    {
        return Order::with('orderedItems.item', 'customer')->get();
    }

    // if (!empty($request->start_date) && !empty($request->end_date)) {
    //     $start = request()->start_date;
    //     $end =  request()->end_date;
    //     $transaction_data->whereDate('transaction_date', '>=', $start)
    //                 ->whereDate('transaction_date', '<=', $end);
    //   }
}
