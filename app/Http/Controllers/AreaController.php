<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use Yajra\DataTables\DataTables;

use function GuzzleHttp\Promise\all;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $area = DB::table('districts')->all()
            // ->join('areas', 'districts.id', '=', 'areas.district_id')->where('areas.id', 1)
            // ->select('areas.id', 'areas.area_name', 'districts.name_en')
            // ->get();
            // dd($area);
        return Inertia::render('Area/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = DB::select('select * from districts');
        // dd($districts);
        return Inertia::render('Area/CreateUpdate', compact('districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'area_name' => ['required', 'max:50'],
            'disc_id' => ['required', 'max:50'],

        ]);

        try {
            DB::beginTransaction();

            $newArea = new Area();

            $newArea->area_name = $request->area_name;
            $newArea->district_id = $request->disc_id;

            $newArea->save();

            DB::commit();

            return redirect(route('area.index'));
        } catch (Exception $ex) {
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        $area = DB::table('areas')->where('areas.id', $id)
            ->leftJoin('districts', 'districts.id', '=', 'areas.district_id')
            ->select('areas.id', 'areas.area_name', 'districts.name_en')
            ->get();
        return Inertia::render('Area/CreateUpdate', [
            'editarea' => $area,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $id = $request->id;
        $request->validate([
            'area_name' => ['required', 'max:50'],
            'disc_id' => ['required', 'max:50'],

        ]);

        try {
            DB::beginTransaction();

            $area = Area::find('id', $id);

            $area->area_name = $request->area_name;
            $area->district_id = $request->disc_id;

            $area->save();

            DB::commit();

            return redirect(route('area.index'));
        } catch (Exception $ex) {
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        try {
            Area::find($area->id)->delete();
            return redirect(route('area.index'));
        } catch (Exception $ex) {
            return redirect(route('area.index'))->with('status', 500);
        }

    }

    public function getData()
    {
        $areas = DB::table('areas')
            ->leftJoin('districts', 'districts.id', '=', 'areas.district_id')
            ->select('areas.id', 'areas.area_name', 'districts.name_en')
            ->get();

        return DataTables::of($areas)
            ->addColumn('action', function ($row) {
                return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item action_edit" data-user-id="' . $row->id . '" href="javascript:void(0)"><i class="fas fa-edit mr-2"></i> Edit</a>
                                <a class="dropdown-item text-danger action_delete" data-user-id="' . $row->id . '" href="javascript:void(0)"><i class="fas fa-trash mr-2"></i> Delete</a>
                                </div>
                            </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
