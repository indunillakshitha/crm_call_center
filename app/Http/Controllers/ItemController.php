<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use Yajra\DataTables\Facades\DataTables;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Items/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Items/CreateUpdate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'item_name' => ['required', 'max:50'],
            'item_description' => ['required', 'max:50'],
            'item_price' => ['required', 'max:50'],
        ]);

        try {
            DB::beginTransaction();

            $newItem = new Item();

            $newItem->item_name = $request->item_name;
            $newItem->item_description = $request->item_description;
            $newItem->item_price = $request->item_price;

            $newItem->save();

            DB::commit();

            return redirect(route('products.index'));
        } catch (Exception $ex) {
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::where('id', $id)->firstOrFail();
        return Inertia::render('Items/CreateUpdate', [
            'edititem' => $item,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $item_id = $request->id;

        $request->validate([
            'item_name' => ['required', 'max:50'],
            'item_description' => ['required', 'max:50'],
            'item_price' => ['required', 'max:50'],
        ]);

        try {
            DB::beginTransaction();

            $Item = Item::find($item_id);
            $Item->item_name = $request->item_name;
            $Item->item_description = $request->item_description;
            $Item->item_price = $request->item_price;
            $Item->save();

            DB::commit();

            return redirect(route('products.index'));

        } catch (Exception $ex) {
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            Item::find($request->id)->delete();
            return redirect(route('products.index'));
        } catch (Exception $ex) {
            return redirect(route('products.index'))->with('status', 500);
        }

    }

    public function getData()
    {
        $items = Item::all();

        return DataTables::of($items)
            ->addColumn('action', function ($row) {
                return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item action_edit" data-user-id="' . $row->id . '" href="javascript:void(0)"><i class="fas fa-edit mr-2"></i> Edit</a>
                                <a class="dropdown-item text-danger action_delete" data-user-id="' . $row->id . '" href="javascript:void(0)"><i class="fas fa-trash mr-2"></i> Delete</a>
                                </div>
                            </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
