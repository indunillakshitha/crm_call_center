<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardUtilityController extends Controller
{
    public function getData(Request $request){
        // return $request;

        $now = Carbon::now();
        $today = $now->toDateString();
        $month = $now->month;
        $year = $now->year;

        if($request->status == 'INQUIRY' ||$request->status == 'NEW' ){
            $find_column = 'order_date';
        } else {
            $find_column = 'deliver_date';
        }


        $count_today = Order::where('status', $request->status)->where($find_column, $today)->count();
        $count_this_month = Order::where('status', $request->status)->whereMonth($find_column, $month)->count();
        $count_this_year = Order::where('status', $request->status)->whereYear($find_column, $year)->count();



        return compact('count_today', 'count_this_month', 'count_this_year');
    }
}
