<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use Yajra\DataTables\Facades\DataTables;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Customer/index', [
            'table_buttons' => ['csv', 'excel', 'pdf', 'print'],
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Order/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'address' => ['required', 'max:50'],
            'email' => ['required', 'max:50', 'email'],
            'mobile' => ['required', 'numeric'],
            'nic' => ['required', 'string'],
        ]);

        try {
            DB::beginTransaction();

            $newCus = new Customer();

            $newCus->first_name = $request->first_name;
            $newCus->last_name = $request->last_name;
            $newCus->address = $request->address;
            $newCus->email = $request->email;
            $newCus->mobile = $request->mobile;
            $newCus->nic = $request->nic;
            $newCus->save();

            DB::commit();

            return redirect(route('customer.index'));

        } catch (Exception $ex) {
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cus = Customer::where('id', $id)->firstOrFail();
        return Inertia::render('Customer/CreateUpdate', [
            'editcus' => $cus,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $request->validate([
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'address' => ['required', 'max:50'],
            'email' => ['required', 'max:50', 'email'],
            'mobile' => ['required', 'numeric'],
            'nic' => ['required', 'string'],
        ]);

        try {
            DB::beginTransaction();

            $cus = Customer::find($request->id);
            $cus->first_name = $request->first_name;
            $cus->last_name = $request->last_name;
            $cus->address = $request->address;
            $cus->email = $request->email;
            $cus->mobile = $request->mobile;
            $cus->nic = $request->nic;
            $cus->save();

            DB::commit();

            return redirect(route('customer.index'));
        } catch (Exception $ex) {
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            Customer::find($request->id)->delete();
            return redirect(route('customer.index'));
        } catch (Exception $ex) {
            return redirect(route('customer.index'))->with('status', 500);
        }
    }

    public function getData()
    {

        $customers = Customer::all();

        return DataTables::of($customers)
            ->addColumn('action', function ($row) {
                return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item action_edit" data-user-id="' . $row->id . '" href="javascript:void(0)"><i class="fas fa-edit mr-2"></i> Edit</a>
                                <a class="dropdown-item text-danger action_delete" data-user-id="' . $row->id . '" href="javascript:void(0)"><i class="fas fa-trash mr-2"></i> Delete</a>
                                </div>
                            </div>';
            })->addColumn('status', function ($row) {
                if ($row->is_active == 1) {
                    return '<span class="badge badge-success badge-pill">Active</span>';
                } else {
                    return '<span class="badge badge-danger badge-pill">Inactive</span>';
                }
            })->rawColumns(['action', 'status'])
            ->make(true);

    }

    public function seacrchCustomer(Request $request){
        
        $customer =Customer::where('mobile',$request->customer_mobile)
                            ->orWhere('land_phone',$request->customer_mobile)                    
                            ->first();

        if($customer!=null){

            return response()->json(['status'=>'success',$customer]);
        }else{
            return response()->json(['status'=>'customer not found']);

        }
    }

    public function updateStatus(){

        $user = Customer::find($request->id);
        $status = $user->is_active == 1 ? 0 : 1;
        $user->update(['is_active' => $status]);

        return redirect(route('customer.index'));
    }
}
