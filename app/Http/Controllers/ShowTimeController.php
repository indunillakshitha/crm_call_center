<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\ShowTimes;
use DateTime;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Yajra\DataTables\Facades\DataTables;

class ShowTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('ShowTimes/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $all_channels = Channel::all();
        // dd($all_channels);
        return Inertia::render('ShowTimes/CreateUpdate' ,compact('all_channels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        ShowTimes::create($request->all());

        return redirect(route('show.times.create'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getData(){
         $showtimes = ShowTimes::leftjoin('channels','channels.id','show_times.channel_id')
                            ->select('show_times.*','channels.channel_name')
                            ->get();

        // if($status != 'get_all_statuses'){
        //     $orders = $orders->where('status', $status);
        // }

        return DataTables::of($showtimes)
            ->addColumn('action', function ($row) {
                return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item text-danger action_view" data-order-id="' . $row->id . '" href="javascript:void(0)" ><i class="fas fa-eye mr-2" ></i> VIEW</a>
                                </div>
                            </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
