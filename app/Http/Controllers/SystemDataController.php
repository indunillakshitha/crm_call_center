<?php

namespace App\Http\Controllers;

use App\Models\SystemData;
use Illuminate\Http\Request;

class SystemDataController extends Controller
{
    //
    public function __construct()
    {
         
        $this->dbVersion = config('app.db_version');
    }

    public function install(Request $request)
    {
        $is_installed = SystemData::getProperty('db_version');
        if (!empty($is_installed)) {
            abort(404);
        }

        DB::statement('SET default_storage_engine=INNODB;');
        Artisan::call('migrate');
        Artisan::call('seed');
        System::addProperty('db_version', $this->dbVersion);

    }

    private function installSettings()
    {
        config(['app.debug' => true]);
        Artisan::call('config:clear');
        Artisan::call('cache:clear');
    }

    // public function instalsl()
    // {
         

    //     $is_installed = System::getProperty($this->module_name . '_version');
    //     if (!empty($is_installed)) {
    //         abort(404);
    //     }

    //     DB::statement('SET default_storage_engine=INNODB;');
    //     Artisan::call('module:migrate', ['module' => "Accounting"]);
    //     Artisan::call('module:seed', ['module' => "Accounting"]);
    //     System::addProperty('db_version', $this->dbVersion);

    //     $output = ['success' => 1,
    //                 'msg' => 'Accounting module installed succesfully'
    //             ];

    //     return redirect()
    //         ->action('\App\Http\Controllers\Install\ModulesController@index')
    //         ->with('status', $output);
    // }
}
