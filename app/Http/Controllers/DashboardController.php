<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Item;
use Inertia\Inertia;
use App\Models\Order;
use App\Models\Channel;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //
    public function index()
    {

        //auth()->user()->givePermissionTo('access.dashboard');
 
        if (!auth()->user()->can('access.dashboard')) {
            abort(403, 'Unauthorized action.');
        }

        

        $total=Order::where('agent_id',Auth::user()->id)->count();
        $total_new=Order::where('status','NEW')->where('agent_id',Auth::user()->id)->count();
        $total_dispatched=Order::where('status','DISPATCHED')->where('agent_id',Auth::user()->id)->count();
        $total_delivered=Order::where('status','DELIVERED')->where('agent_id',Auth::user()->id)->count();
        $status = 'get_all_statuses';

        $all_orders = Order::all();
        $to_be_dispatched_today = Order::where('status', 'NEW')->whereDate('deliver_date', '<=', Carbon::now()->toDateString())->count();
        $to_be_dispatched_tomorrow = Order::where('status', 'NEW')->whereDate('deliver_date', '=', Carbon::tomorrow()->toDateString())->count();
        $to_be_dispacthed_total = $to_be_dispatched_today + $to_be_dispatched_tomorrow;

        return Inertia::render('Dashboard', compact('status','total','total_new','total_dispatched','total_delivered', 'all_orders', 'to_be_dispacthed_total'));
    }

    public function getDashboardData(Request $req)
    {   
        if (!auth()->user()->can('access.dashboard')) {
            abort(403, 'Unauthorized action.');
        }

        $filter_by = $req->filter;
        $now = Carbon::now();
        $today = $now->toDateString();
        
        $tomorrowDate = Carbon::tomorrow();   
        $tomorrow = $tomorrowDate->toDateString(); 

        $month = $now->month;
        $year = $now->year;

        $list_of_counts =[];
        
        //New Customers
        if (auth()->user()->can('access.dashboard')) {
            $newCustomers=0;
            if($filter_by=='today'){
                $newCustomers = Customer::whereDate('created_at',$today)->count();
            }else if($filter_by=='month'){
                $newCustomers = Customer::whereMonth('created_at',$month)->count();
            }else{
                $newCustomers = Customer::count();
            }
            $list_of_counts[] =['title'=>'New Customers','count'=>$newCustomers, 'class'=>'round align-self-center round-success','icon'=>'fa fas fa-users'];
        }

        //Inquiries
        if (auth()->user()->can('access.dashboard')) {
            $inquiries=0;
            if($filter_by=='today'){
                $inquiries = Order::whereDate('order_date',$today)->count();
            }else if($filter_by=='month'){
                $inquiries = Order::whereMonth('order_date',$month)->count();
            }else{
                $inquiries = Order::count();
            }
            $list_of_counts[] =['title'=>'INQUIRIES','count'=>$inquiries, 'class'=>'round align-self-center round-info','icon'=>'fa fa-phone'];
        }

        //Orders
        if (auth()->user()->can('access.dashboard')) {
            $orders=0;
            if($filter_by=='today'){
                $orders = Order::whereDate('order_date',$today)->where('status_id',2)->count();
            }else if($filter_by=='month'){
                $orders = Order::whereMonth('order_date',$month)->where('status_id',2)->count();
            }else{
                $orders = Order::where('status_id',2)->count();
            }
            $list_of_counts[] =['title'=>'ORDERS','count'=>$orders, 'class'=>'round align-self-center bg-info','icon'=>'fas fa-cart-arrow-down'];
        }

        //Delivered
        if (auth()->user()->can('access.dashboard')) {
            $delivered=0;
            if($filter_by=='today'){
                $delivered = Order::whereDate('order_date',$today)->where('status_id',4)->count();
            }else if($filter_by=='month'){
                $delivered = Order::whereMonth('order_date',$month)->where('status_id',4)->count();
            }else{
                $delivered = Order::where('status_id',4)->count();
            }
            $list_of_counts[] =['title'=>'DELIVERED','count'=>$delivered, 'class'=>'round align-self-center round-success','icon'=>'fas fa-shipping-fast'];
        }

         //Completed
         if (auth()->user()->can('access.dashboard')) {
            $completed=0;
            if($filter_by=='today'){
                $completed = Order::whereDate('order_date',$today)->where('status_id',5)->count();
            }else if($filter_by=='month'){
                $completed = Order::whereMonth('order_date',$month)->where('status_id',5)->count();
            }else{
                $completed = Order::where('status_id',5)->count();
            }
            $list_of_counts[] =['title'=>'COMPLETED','count'=>$completed, 'class'=>'round align-self-center round-purple','icon'=>'fas fa-clipboard-check'];
        }

         //In Transit
         if (auth()->user()->can('access.dashboard')) {
            $in_transit=0;
            if($filter_by=='today'){
                $in_transit = Order::whereDate('order_date',$today)->where('status_id',3)->count();
            }else if($filter_by=='month'){
                $in_transit = Order::whereMonth('order_date',$month)->where('status_id',3)->count();
            }else{
                $in_transit = Order::where('status_id',3)->count();
            }
            $list_of_counts[] =['title'=>'IN TRANSIT','count'=>$in_transit, 'class'=>'round align-self-center round-dark','icon'=>'fas fa-box'];
        }
         
         //Inquiry Follow Up
         if (auth()->user()->can('access.dashboard')) {
             
            $inquiry_follow_up = Order::whereDate('deliver_date','<=',$tomorrow)->where('status_id',1)->count();
            $list_of_counts[] =['title'=>'INQUIRIES FOLLOW UP','count'=>$inquiry_follow_up, 'class'=>'round align-self-center round-info','icon'=>'ti-user'];
        }

        //Orders Follow Up
        if (auth()->user()->can('access.dashboard')) {
             
            $order_follow_up = Order::whereDate('deliver_date','<=',$tomorrow)->where('status_id',2)->count();
            $list_of_counts[] =['title'=>'ORDERES FOLLOW UP','count'=>$order_follow_up, 'class'=>'round align-self-center round-danger','icon'=>'ti-calendar'];
        }


        //Inquiries by chanels
        $donutChartData =null;
        if (auth()->user()->can('access.dashboard')) {
            $chart_data=[];
            if($filter_by=='today'){

                $chart_data = Channel::withCount(['orders' => function($query) use($today){
                    $query->whereDate('orders.order_date',$today);
                }])->get();               
            }else if($filter_by=='month'){

                $chart_data = Channel::withCount(['orders' => function($query) use($month){
                    $query->whereMonth('orders.order_date',$month);
                }])->get();
            }else{
                $chart_data = Channel::withCount(['orders'])->get();
            }

            $lable_list=[];
            $color_list=[];

            foreach($chart_data as $chanell){
                $lable_list[]= ['label'=>$chanell->channel_name,'value'=>$chanell->orders_count,'color'=>$chanell->color];
                $color_list[]= [$chanell->color];
            }

            $donutChartData=[
                'data'=>$lable_list,
                'color'=>$color_list
            ];
            
        }

        $areaChartData =null;
        if (auth()->user()->can('access.dashboard')) {
            $chart_data=[];
            if($filter_by=='today'){
                // $chart_data = Item::join('ordered_items AS OI','OI.item_id','=','item.id')
                //                   ->join('orders','orders.id','=','OI.order_id')
                //                   ->select()->distinct('')->get();
                $chart_data = Item::select('id','item_name','item_description',
                                DB::raw("(SELECT SUM(OI.product_qty) as total_qty FROM ordered_items as OI 
                                        INNER JOIN orders as OD ON OD.id = OI.order_id WHERE OI.item_id=items.id AND DATE(OD.order_date) = $today ) AS total_qty")
                                )->get();
                
              //  return $chart_data;          
            }else if($filter_by=='month'){
                $chart_data = Item::select('id','item_name','item_description',
                DB::raw("(SELECT SUM(OI.product_qty) as total_qty FROM ordered_items as OI 
                         INNER JOIN orders as OD ON OD.id = OI.order_id WHERE OI.item_id=items.id AND MONTH(OD.order_date) = $month ) AS total_qty")
                )->get();

               // return $chart_data;
            }else{
                $chart_data = Item::select('id','item_name','item_description',
                                DB::raw("(SELECT SUM(OI.product_qty) as total_qty FROM ordered_items as OI 
                                         INNER JOIN orders as OD ON OD.id = OI.order_id WHERE OI.item_id=items.id) AS total_qty")
                                )->get();
             //   return $chart_data;
            }

            $data=[];
            $color_list=[];

            foreach($chart_data as $item){
                $lable_list[]= ['label'=>$item->item_name,'value'=>$item->total_qty];
                $color_list[]= [$item->color];
            }

            $areaChartData=[
                'data'=>$lable_list,
                'color'=>$color_list
            ];
            
        }
        
        

        return ['count_data'=>$list_of_counts,'donutchart_data'=>$donutChartData, 'areachart_data'=>$areaChartData];
    }

     
}
