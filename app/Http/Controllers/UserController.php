<?php

namespace App\Http\Controllers;

use Exception;
use DataTables;
use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //

    public function index()
    {
        if (!auth()->user()->can('users.view')) {
            abort(403, 'Unauthorized action.');
        }
        auth()->user()->assignRole('Super Admin');
        return Inertia::render('Users/Index',[
            'table_buttons'=>['csv', 'excel', 'pdf', 'print']
        ]);
    }

    public function create()
    {
        if (!auth()->user()->can('users.add')) {
            abort(403, 'Unauthorized action.');
        }
        return Inertia::render('Users/CreateUpdate');
    }

    public function edit($id)
    {

        if (!auth()->user()->can('users.edit')) {
            abort(403, 'Unauthorized action.');
        }

        $user = User::where('id',$id)
                    ->where('user_account_type_id', config('custom_config.user_account_types_id.user'))
                    ->select('id','first_name','last_name','name as username','email','mobile')->firstOrFail();

        return Inertia::render('Users/CreateUpdate',[
            'edituser'=>$user
        ]);
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('users.add')) {
            abort(403, 'Unauthorized action.');
        }

       $request->validate([
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'username'=> ['required', 'max:50', 'string','unique:users,name'],
            'email' => ['required', 'max:50', 'email','unique:users'],
            'password' => ['required','string','confirmed','min:8'],
            'mobile' => ['required','numeric'],
        ]);
        
        try{
            DB::beginTransaction();

            $newUser = new User();

            $newUser->first_name = $request->first_name;
            $newUser->last_name = $request->last_name;
            $newUser->name = $request->username;
            $newUser->email = $request->email;
            $newUser->password = Hash::make($request->password);
            $newUser->mobile = $request->mobile;
            $newUser->user_type_id = isset($request->user_type) ? $request->user_type : 1;
            $newUser->user_account_type_id = 3; //Client User
            $newUser->save();

            DB::commit();

            return redirect(route('user.index'));

        }catch(Exception $ex){
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }

    }

    public function update(Request $request)
    {
        if (!auth()->user()->can('users.edit')) {
            abort(403, 'Unauthorized action.');
        }

        $user_id = $request->id;

       $request->validate([
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'username'=> ['required', 'max:50', 'string','unique:users,name,'.$user_id],
            'email' => ['required', 'max:50', 'email','unique:users,email,'.$user_id],
            'mobile' => ['required','numeric'],
        ]);

        if(isset($request->password)){
            $request->validate([
                'password' => ['required','string','confirmed','min:8'],
            ]);
        }


        try{
            DB::beginTransaction();

            $User = User::find($user_id);

            $User->first_name = $request->first_name;
            $User->last_name = $request->last_name;
            $User->name = $request->username;
            $User->email = $request->email;
            $User->mobile = $request->mobile;

            if(isset($request->password)){
                $User->password = Hash::make($request->password);
            }

            $User->save();


            DB::commit();

            return redirect(route('user.index'));

        }catch(Exception $ex){
            DB::rollback();
            \Log::error($ex);
            return abort(500);
        }

    }

    public function getData(Request $request)
    {

        $users = User::join('user_types','user_types.id','=','users.user_type_id')
                        ->where('user_account_type_id',config('custom_config.user_account_types_id.user'))
                        ->select('users.id','users.first_name','users.last_name','users.email','users.mobile','users.rating','users.is_active','user_types.user_type');
                        //->get();

        return Datatables::of($users)
                    ->addColumn('action',  function ($row) {

                        $action_html='';

                        if(auth()->user()->can('users.view')){
                            $action_html.='<a class="dropdown-item action_view" data-user-id="'.$row->id.'"  href="javascript:void(0)"><i class="fas fa-eye mr-2"></i> View</a>';
                        }
                        if(auth()->user()->can('users.view.history')){
                            $action_html.='<a class="dropdown-item action_view_history" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-history mr-2"></i> History</a>';                        }
                        if(auth()->user()->can('users.edit')){
                            $action_html.='<a class="dropdown-item action_edit" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-edit mr-2"></i> Edit</a>';
                        }
                        if(auth()->user()->can('users.change.status') || auth()-user()->can('users.change.delete')){
                            $action_html.='<div class="dropdown-divider"></div>';
                        }
                        if(auth()->user()->can('users.change.status')){
                            $action_html.='<a class="dropdown-item '.($row->is_active==1 ? 'text-warning' : 'text-success').' action_status_change" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-power-off mr-2"></i>'.($row->is_active==1 ? 'Deactivate' : 'Activate').'</a> ';
                        }
                        if(auth()->user()->can('users.delete')){
                            $action_html.='<a class="dropdown-item text-danger action_delete" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-trash mr-2"></i> Delete</a> ';
                        }
                        return '<div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                '.$action_html.'
                                </div>
                            </div>';

                     })->addColumn('status', function($row){
                         if($row->is_active==1){
                             return '<span class="badge badge-success badge-pill">Active</span>';
                         }else{
                            return '<span class="badge badge-danger badge-pill">Inactive</span>';
                         }
                     })->rawColumns(['action','status'])
                    ->make(true);
    }

    public function updateStatus(Request $request)
    {
        $user = User::find($request->id);
        $status = $user->is_active== 1 ? 0 : 1;
        $user->update(['is_active'=> $status ]);

        return redirect(route('user.index'));
    }

    public function destroy(Request $request)
    {
        try{
            User::find($request->id)->delete();
            return redirect(route('user.index'));
        }catch(Exception $ex){
            return redirect(route('user.index'))->with('status',500);
        }

    }

}
