<?php

namespace App\Http\Controllers;

use DataTables;
use Exception;
use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Models\Utility\PermissionsUtility;

class BackendUserController extends Controller
{
    public function validateBackendUsers($request)
    {
        return $request->validate([
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'username' => ['required', 'max:50', 'string', 'unique:users,name,'.$request->id],
            'email' => ['required', 'max:50', 'email', 'unique:users,email,'.$request->id],
            'password' => ['required', 'string', 'confirmed', 'min:8'],
            'mobile' => ['required', 'numeric'],
            'role_name' => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        PermissionsUtility::checkPermission('providers.view');
        return Inertia::render('BackEndUsers/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        PermissionsUtility::checkPermission('providers.add');
        $all_roles = Role::all();
        return Inertia::render('BackEndUsers/CreateUpdate', compact('all_roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        PermissionsUtility::checkPermission('providers.add');

        $this->validateBackendUsers($request);

       try{

        DB::beginTransaction();

        $newUser = new User();
        $newUser->first_name = $request->first_name;
        $newUser->last_name = $request->last_name;
        $newUser->name = $request->username;
        $newUser->email = $request->email;
        $newUser->password = Hash::make($request->password);
        $newUser->mobile = $request->mobile;
        $newUser->user_type_id = isset($request->user_type) ? $request->user_type : 1;
        $newUser->user_account_type_id = 1; //Backend User
        $newUser->save();

        $newUser->syncRoles($request->role_name);

        DB::commit();

        return redirect(route('backend.users.index'));

       }catch(Exception $e) {
            DB::rollback();
            \Log::error($e);
            abort(500);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        PermissionsUtility::checkPermission('providers.edit');
        $edit_backend_user = User::with('roles')->find($id);
        // return $edit_backend_user->roles->first()->name;
        $all_roles = Role::all();

        return Inertia::render('BackEndUsers/CreateUpdate', compact('edit_backend_user', 'all_roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        PermissionsUtility::checkPermission('providers.edit');
        $this->validateBackendUsers($request);

        try{

            DB::beginTransaction();
            User::find($request->id)->fill($request->all())->syncRoles($request->role_name)->save();
            DB::commit();
            return redirect(route('backend.users.index'));

        } catch(Exception $e){
            DB::rollback();
            \Log::error($e);
            abort(500);
        }

    }

    public function updateStatus(Request $request){
        PermissionsUtility::checkPermission('providers.edit');
        $user = User::find($request->id);
        $status = $user->is_active== 1 ? 0 : 1;
        $user->update(['is_active'=> $status ]);

        return redirect(route('backend.users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)

    {
        PermissionsUtility::checkPermission('providers.delete');
        try{
            User::find($request->id)->delete();
            return redirect(route('backend.users.index'));
        }catch(Exception $ex){
            return redirect(route('backend.users.index'))->with('status',500);
        }
    }

    public function getData(Request $request){
        PermissionsUtility::checkPermission('providers.view');
        {
            // $users = User::join('user_types','user_types.id','=','users.user_type_id')
            //                 ->where('user_account_type_id',config('custom_config.user_account_types_id.backend'))
            //                 ->select('users.id','users.first_name','users.last_name','users.email','users.mobile','users.rating','users.is_active','user_types.user_type');
                            //->get();
            $users = User::where('is_active', 1)->get();

            return DataTables::of($users)
                        ->addColumn('action',  function ($row) {

                            $action_html='';


                            //History
                            if(auth()->user()->can('providers.view.history')){
                                $action_html.='<a class="dropdown-item action_view_history" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-history mr-2"></i> History</a>';
                            }

                            //Edit
                            if(auth()->user()->can('providers.edit')){
                                $action_html.='<a class="dropdown-item action_edit" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-edit mr-2"></i> Edit</a>';
                            }

                            //Divider
                            if(auth()->user()->can('providers.change.status') || auth()-user()->can('users.change.delete')){
                                $action_html.='<div class="dropdown-divider"></div>';
                            }

                            //Change Status
                            if(auth()->user()->can('providers.change.status')){
                                $action_html.='<a class="dropdown-item '.($row->is_active==1 ? 'text-warning' : 'text-success').' action_status_change" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-power-off mr-2"></i>'.($row->is_active==1 ? 'Deactivate' : 'Activate').'</a> ';
                            }

                            //Delete
                            if(auth()->user()->can('providers.delete')){
                                $action_html.='<a class="dropdown-item text-danger action_delete" data-user-id="'.$row->id.'" href="javascript:void(0)"><i class="fas fa-trash mr-2"></i> Delete</a> ';
                            }


                            return '<div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>
                                    <div class="dropdown-menu">
                                    '.$action_html.'
                                    </div>
                                </div>';

                         })->addColumn('status', function($row){
                             if($row->is_active==1){
                                 return '<span class="badge badge-success badge-pill">Active</span>';
                             }else{
                                return '<span class="badge badge-danger badge-pill">Inactive</span>';
                             }
                         })
                         ->addColumn('user_role', function($row){
                            $has_role = $row->roles->first();
                            if($has_role){
                                return $has_role->name;
                            } else {
                                return 'No Role Assigned';
                            }
                         })
                         ->rawColumns(['action','status', 'user_role'])
                        ->make(true);
        }
    }
}
