<?php

namespace App\Http\Controllers;

use App\Models\OrderedItems;
use Illuminate\Http\Request;

class OrderedItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderedItems  $orderedItems
     * @return \Illuminate\Http\Response
     */
    public function show(OrderedItems $orderedItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderedItems  $orderedItems
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderedItems $orderedItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderedItems  $orderedItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderedItems $orderedItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderedItems  $orderedItems
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderedItems $orderedItems)
    {
        //
    }
}
