<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Database\Seeders\DefaulDataSeeder;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // Role::create(['name' => 'Super Admin']);

        $permission_array =[
            ['section_name'=>'dashboard','name' => 'access.dashboard'],
            ['section_name'=>'dashboard','name' => 'access.filtering'],

            //Users Section
            ['section_name'=>'user','name' => 'users.view'],
            ['section_name'=>'user','name' => 'users.add'],
            ['section_name'=>'user','name' => 'users.edit'],
            ['section_name'=>'user','name' => 'users.delete'],
            ['section_name'=>'user','name' => 'users.change.status'],
            ['section_name'=>'user','name' => 'users.view.history'],

            //Master Data Section

            //Roles Section
            ['section_name' => 'role', 'name' => 'roles.view'],
            ['section_name' => 'role', 'name' => 'roles.create'],
            ['section_name' => 'role', 'name' => 'roles.edit'],
            ['section_name' => 'role', 'name' => 'roles.delete'],
            ['section_name' => 'role', 'name' => 'roles.change.status'],

            //orders
            ['section_name' => 'order', 'name' => 'orders.view'],
            ['section_name' => 'order', 'name' => 'orders.create'],
            ['section_name' => 'order', 'name' => 'orders.edit'],
            ['section_name' => 'order', 'name' => 'orders.delete'],
            ['section_name' => 'order', 'name' => 'orders.change.status'],

            //customers
            ['section_name' => 'customer', 'name' => 'customers.view'],
            ['section_name' => 'customer', 'name' => 'customers.create'],
            ['section_name' => 'customer', 'name' => 'customers.edit'],
            ['section_name' => 'customer', 'name' => 'customers.delete'],
            ['section_name' => 'customer', 'name' => 'customers.change.status'],

            //inquiries
            ['section_name' => 'inquiry', 'name' => 'inquiries.view'],
            ['section_name' => 'inquiry', 'name' => 'inquiries.create'],
            ['section_name' => 'inquiry', 'name' => 'inquiries.edit'],
            ['section_name' => 'inquiry', 'name' => 'inquiries.delete'],
            ['section_name' => 'inquiry', 'name' => 'inquiries.change.status'],

            //products
            ['section_name' => 'products', 'name' => 'products.view'],
            ['section_name' => 'products', 'name' => 'products.create'],
            ['section_name' => 'products', 'name' => 'products.edit'],
            ['section_name' => 'products', 'name' => 'products.delete'],
            ['section_name' => 'products', 'name' => 'products.change.status'],

            //showtimes
            ['section_name' => 'showtimes', 'name' => 'showtimes.view'],
            ['section_name' => 'showtimes', 'name' => 'showtimes.create'],
            ['section_name' => 'showtimes', 'name' => 'showtimes.edit'],
            ['section_name' => 'showtimes', 'name' => 'showtimes.delete'],
            ['section_name' => 'showtimes', 'name' => 'showtimes.change.status'],


        ];

        foreach($permission_array as $permission){

                $check_has_permission = Permission::where('name', $permission['name'])->first();
                if(!isset($check_has_permission)){
                    Permission::create($permission);
                }

        }

        $this->call([
            // DefaulDataSeeder::class,
        ]);


    }
}
