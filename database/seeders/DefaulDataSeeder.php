<?php

namespace Database\Seeders;

use App\Models\UserType;
use App\Models\UserAccountType;
use Illuminate\Database\Seeder;

class DefaulDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        UserType::create(['user_type'=>'NORMAL']);
        UserType::create(['user_type'=>'BUSINESSUSER']);

        UserAccountType::create(['account_type'=>'Backend User']);
        UserAccountType::create(['account_type'=>'Provider User']);
        UserAccountType::create(['account_type'=>'User']);


    }
}
