<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCustomerT extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function(Blueprint $table){

            $table->string('mobile')->change();
            $table->string('land_phone')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('customers', function(Blueprint $table){
        //     // $table->dropColumn('mobile');
        //     // $table->dropColumn('land_phone');
        //     $table->string('mobile')->unique();
        //     $table->string('land_phone')->nullable();
        // // });
    }
}
