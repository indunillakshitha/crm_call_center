<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('mobile',15)->nullable();
            $table->string('name');
            $table->string('email')->unique();            
            //$2y$10$0XvQSXN09b9l8deE92Hpt.229F.o1ty78gUGQb85Gup/CVvGbqXNq
            $table->bigInteger('user_type_id')->unsigned()->index()->nullable();  
            $table->foreign('user_type_id')->references('id')->on('user_types');

            $table->bigInteger('user_account_type_id')->unsigned()->index()->nullable();  
            $table->foreign('user_account_type_id')->references('id')->on('user_account_types');


            $table->double('rating',2,2)->default(0.00);
            
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
