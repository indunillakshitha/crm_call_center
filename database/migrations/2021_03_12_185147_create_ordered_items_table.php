<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderedItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordered_items', function (Blueprint $table) {
            $table->id();
            //$table->text('order_id')->nullable();

            $table->bigInteger('order_id')->unsigned()->index();  
            $table->foreign('order_id')->references('id')->on('orders');

            $table->bigInteger('item_id')->unsigned()->index();  
            $table->foreign('item_id')->references('id')->on('items');
                       
            $table->decimal('product_qty',22,2);
            $table->decimal('unit_price',22,2);             
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordered_items');
    }
}
