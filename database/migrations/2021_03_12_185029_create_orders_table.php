<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_number',10)->nullable();
             
            $table->bigInteger('customer_id')->unsigned()->index();  
            $table->foreign('customer_id')->references('id')->on('customers');

            
            $table->date('order_date');
            $table->date('deliver_date')->nullable();
            $table->string('source',15);
                    
            $table->bigInteger('channel_id')->unsigned()->index();  
            $table->foreign('channel_id')->references('id')->on('channels');

            $table->bigInteger('show_time_id')->unsigned()->index();  
            $table->foreign('show_time_id')->references('id')->on('show_times');

           

            //$table->dateTime('show_time')->nullable();
            // $table->text('order_detail_id')->nullable();
            $table->string('status')->nullable();
            $table->string('executive_ramarks',500)->nullable();
            // $table->text('dealer_name')->nullable();
            // $table->text('agent_name')->nullable();
            // $table->text('agent_id')->nullable();
            $table->bigInteger('agent_id')->unsigned()->index();  
            $table->foreign('agent_id')->references('id')->on('users');

            $table->string('contact_1',10)->nullable();
            $table->string('contact_2',10)->nullable();
            $table->string('address',250)->nullable();
            // $table->text('area')->nullable();

            $table->bigInteger('area_id')->unsigned()->index();  
            $table->foreign('area_id')->references('id')->on('areas');

            $table->bigInteger('district_id')->unsigned()->index();  
            $table->foreign('district_id')->references('id')->on('districts');

            
            $table->decimal('order_total',22,2)->default(0);


            //$table->text('district')->nullable();
            $table->string('remark',250)->nullable();
            $table->tinyInteger('is_hold')->nullable();
            
            $table->bigInteger('status_id')->unsigned()->index();  
            $table->foreign('status_id')->references('id')->on('statuses');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
