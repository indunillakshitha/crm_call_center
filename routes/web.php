<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\InquiryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ShowTimeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\BackendUserController;
use App\Http\Controllers\DashboardUtilityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

// Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
//     return Inertia::render('Dashboard');
// })->name('dashboard');
//Route::group(['middleware' => ['auth:sanctum', 'verified']], function(){
Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {

    Route::get('/', [DashboardController::class, 'index'])->middleware(['can:access.dashboard'])->name('dashboard');
    Route::get('/dashboard', [DashboardController::class, 'index'])->middleware(['can:access.dashboard'])->name('dashboard');
    Route::POST('/dashboard/get-count-data', [DashboardController::class, 'getDashboardData'])->middleware(['can:access.dashboard'])->name('dashboard.get.count.data');

    //Users (Client)
    Route::get('/users', [UserController::class, 'index'])->middleware(['can:users.view'])->name('user.index');
    Route::get('/users/create', [UserController::class, 'create'])->middleware(['can:users.add'])->name('user.create');
    Route::get('/user/edit/{id}', [UserController::class, 'edit'])->middleware(['can:users.edit'])->name('user.edit');
    Route::post('/users/store', [UserController::class, 'store'])->middleware(['can:users.add'])->name('user.store');
    Route::put('/users/update', [UserController::class, 'update'])->middleware(['can:users.edit'])->name('user.update');
    Route::put('/users/change/status', [UserController::class, 'updateStatus'])->middleware(['can:users.change.status'])->name('user.change.status');
    Route::get('/users/get/data', [UserController::class, 'getData'])->middleware(['can:users.view'])->name('user.getdata');
    Route::delete('/users/delete', [UserController::class, 'destroy'])->middleware(['can:users.delete'])->name('user.delete');

    //MASTER DATA SECTION

    // Backend Users Section
    Route::get('/backend/users', [BackendUserController::class, 'index'])->middleware(['can:backend.users.view'])->name('backend.users.index');
    Route::get('/backend/users/create', [BackendUserController::class, 'create'])->middleware(['can:backend.users.create'])->name('backend.users.create');
    Route::post('/backend/users/store', [BackendUserController::class, 'store'])->middleware(['can:backend.users.store'])->name('backend.users.store');
    Route::post('/backend/users/get/data', [BackendUserController::class, 'getData'])->middleware(['can:backend.users.view'])->name('backend.users.getdata');
    Route::get('/backend/users/get/data', [BackendUserController::class, 'getData'])->middleware(['can:backend.users.view'])->name('backend.users.getdata');
    Route::get('/backend/users/edit/{id}', [BackendUserController::class, 'edit'])->middleware(['can:backend.users.edit'])->name('backend.users.edit');
    Route::put('/backend/users/update', [BackendUserController::class, 'update'])->middleware(['can:backend.users.edit'])->name('backend.users.update');
    Route::put('/backend/users/status', [BackendUserController::class, 'updateStatus'])->middleware(['can:backend.users.change.status'])->name('backend.users.change.status');
    Route::delete('/backend/users/delete', [BackendUserController::class, 'destroy'])->middleware(['can:backend.users.delete'])->name('backend.users.delete');

    //Roles section
    Route::get('/roles', [RoleController::class, 'index'])->middleware(['can:roles.index'])->name('roles.index');
    Route::get('/roles/create', [RoleController::class, 'create'])->middleware(['can:roles.create'])->name('roles.create');
    Route::post('/roles/store', [RoleController::class, 'store'])->middleware(['can:roles.create'])->name('roles.store');
    Route::post('/roles/getdata', [RoleController::class, 'getdata'])->middleware(['can:roles.view'])->name('roles.getdata');
    Route::get('/roles/getdata', [RoleController::class, 'getdata'])->middleware(['can:roles.view'])->name('roles.getdata');
    Route::delete('/roles/delete', [RoleController::class, 'destroy'])->middleware(['can:roles.delete'])->name('roles.delete');
    Route::get('/roles/edit/{id}', [RoleController::class, 'edit'])->middleware(['can:roles.edit'])->name('roles.edit');
    Route::put('/roles/update', [RoleController::class, 'update'])->middleware(['can:roles.edit'])->name('roles.update');

    // Order Routes
    Route::get('/orders', [OrderController::class, 'index'])->name('order.index');
    Route::get('/orders/create', [OrderController::class, 'create'])->name('order.create');
    Route::post('/orders/store', [OrderController::class, 'store'])->name('order.store');
    Route::post('/orders/store/additems', [OrderController::class, 'addItems'])->name('order.additems');
    Route::get('/orders/store/removeitems', [OrderController::class, 'removeItems'])->name('order.removeitems');
    Route::get('/orders/new', [OrderController::class, 'newOrders'])->name('order.new');
    // Route::get('/orders/getdata/{status}', [OrderController::class, 'getData'])->name('order.getdata');
    Route::get('/orders/getdata', [OrderController::class, 'getData2'])->name('order.getdata');
    Route::post('/orders/getdata', [OrderController::class, 'getData2'])->name('order.getdata');
    Route::get('/orders/pending', [OrderController::class, 'pendingOrders'])->name('order.pending');
    // Route::post('/orders/pending/getdata', [OrderController::class, 'getdataNew'])->name('order.pending.getdata');
    Route::get('/orders/dispatched', [OrderController::class, 'dispatchedOrders'])->name('order.dispatched');
    Route::get('/orders/delivered', [OrderController::class, 'deliveredOrders'])->name('order.delivered');
    Route::get('/orders/completed', [OrderController::class, 'completededOrders'])->name('order.completed');
    Route::post('/order/details/', [OrderController::class, 'getOrderDetails'])->name('order.details');
    Route::post('/order/details/forward', [OrderController::class, 'forwardOrder'])->name('order.forward');
    Route::post('/inquiry/toorder', [OrderController::class, 'inquiryToOrder'])->name('order.toinquiry');

    //change order status directly
    Route::post('/order/status/change', [OrderController::class, 'changeStatus'])->name('order.status.change');
    Route::post('/order/status/change/bulk', [OrderController::class, 'changeStatusBulk'])->name('order.status.change.bulk');
    Route::post('/order/search/field', [OrderController::class, 'ordersSearchField'])->name('orders.search.field');
    Route::get('/reltest', [OrderController::class, 'reltest'])->name('orders.reltest');

    //call handller
    Route::get('/customer-search', function () {
        return Inertia::render('CustomerSearch/CusSearch');});

    //customers
    Route::prefix('customers')->group(function () {
        Route::get('/index', [CustomerController::class, 'index'])->name('customer.index');
        Route::get('/create', [CustomerController::class, 'create'])->name('customer.create');
        Route::post('/store', [CustomerController::class, 'store'])->name('customer.store');
        Route::get('/edit/{id}', [CustomerController::class, 'edit'])->name('customer.edit');
        Route::put('/update', [CustomerController::class, 'update'])->name('customer.update');
        Route::get('/get/data', [CustomerController::class, 'getData'])->name('customer.getdata');
        Route::put('/change/status', [UserContrCustomerControllerller::class, 'updateStatus'])->name('customer.change.status');
        Route::post('/search', [CustomerController::class, 'seacrchCustomer'])->name('customer.search');
    });

    //inquiry
    Route::prefix('inquiry')->group(function () {
        Route::get('/index', [InquiryController::class, 'index'])->name('inquiry.index');
        Route::get('/create', [InquiryController::class, 'create'])->name('inquiry.create');
        Route::get('/getdata', [InquiryController::class, 'getData'])->name('inquiry.getdata');
        Route::get('/get/customer/history/data/{id}', [InquiryController::class, 'getCustomerOrderHistory'])->name('inquiry.getCustomerOrderHistory');
    });
    Route::post('/inquiry/create', [InquiryController::class, 'store'])->name('inquiry.store');

    //check inquiry
    Route::post('/inquiry/check', [InquiryController::class, 'checkPhoneNo'])->name('inquiry.checkPhoneNo');

    //products
    Route::prefix('products')->group(function () {
        Route::get('/index', [ItemController::class, 'index'])->name('products.index');
        Route::get('/create', [ItemController::class, 'create'])->name('products.create');
        Route::post('/store', [ItemController::class, 'store'])->name('products.store');
        Route::get('/edit/{id}', [ItemController::class, 'edit'])->name('products.edit');
        Route::put('/update', [ItemController::class, 'update'])->name('products.update');
        Route::get('/get/data', [ItemController::class, 'getData'])->name('products.getdata');
        Route::delete('/delete', [ItemController::class, 'destroy'])->name('products.delete');
    });

    //showtimes
    Route::get('/showtimes/create', [ShowTimeController::class, 'create'])->name('show.times.create');
    Route::post('/showtimes/store', [ShowTimeController::class, 'store'])->name('show.times.store');
    Route::get('/showtimes/getdata', [ShowTimeController::class, 'getData'])->name('show.times.getdata');
    Route::get('/showtimes/index', [ShowTimeController::class, 'index'])->name('show.times.index');

    //Area
    Route::prefix('area')->group(function () {
        Route::get('/index', [AreaController::class, 'index'])->name('area.index');
        Route::get('/create', [AreaController::class, 'create'])->name('area.create');
        Route::post('/store', [AreaController::class, 'store'])->name('area.store');
        Route::get('/edit/{id}', [AreaController::class, 'edit'])->name('area.edit');
        Route::put('/update', [AreaController::class, 'update'])->name('area.update');
        Route::get('/get/data', [AreaController::class, 'getData'])->name('area.getdata');
        Route::delete('/delete', [AreaController::class, 'destroy'])->name('area.delete');
    });

    //Dashboard Widgets
    Route::post('/get/data/dashboard/widgets', [DashboardUtilityController::class, 'getData'])->name('get.data.dashboard.widget');
});
